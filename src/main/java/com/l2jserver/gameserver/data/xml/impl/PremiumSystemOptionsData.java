package com.l2jserver.gameserver.data.xml.impl;

import com.l2jserver.gameserver.model.L2PremiumBonus;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.util.IXmlReader;
import com.l2jserver.gameserver.util.IXmlStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * This class contains a set of different premium subscription options
 * @author netvirus
 */

public class PremiumSystemOptionsData implements IXmlReader, IXmlStreamReader {

    private static final Logger LOG = LoggerFactory.getLogger(PremiumSystemOptionsData.class);

    private final Map<Integer, L2PremiumBonus> _premiumBonusList = new LinkedHashMap<>();

    public PremiumSystemOptionsData() { load(); }

    @Override
    public void load() {
        _premiumBonusList.clear();
        parseDatapackFile("data/premiumSystemOptionsData.xml");
        LOG.info("Loaded {} Premium system profiles.", _premiumBonusList.size());
    }

    @Override
    public void parseDocument(Document doc) {
        nodeListStreamFilteredByName(doc.getFirstChild().getChildNodes(), "premium").forEach(n -> {
            final StatsSet set = new StatsSet();
            attributeStream(n.getAttributes()).forEach(a -> set.set(a.getNodeName(), a.getNodeValue()));
            nodeListStreamFilteredByMap(n.getChildNodes(), Map.of("rates", "rates", "time", "time", "price", "price"))
                    .forEach(p -> attributeStream(p.getAttributes()).forEach(k -> set.set(k.getNodeName(), k.getNodeValue())));
            _premiumBonusList.put(set.getInt("id"), new L2PremiumBonus(set));
        });
    }

    public static PremiumSystemOptionsData getInstance() {
        return PremiumSystemOptionsData.SingletonHolder.INSTANCE;
    }

    public L2PremiumBonus findById(int bonus_id) {
        return _premiumBonusList.get(bonus_id);
    }

    private static class SingletonHolder {
        protected static final PremiumSystemOptionsData INSTANCE = new PremiumSystemOptionsData();
    }
}
