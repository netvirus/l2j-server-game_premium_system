package com.l2jserver.gameserver.util;

/**
 * @author netvirus
 */

public enum TimeUtilDeclension {
    DAYS,
    HOUR,
    MINUTES;

    TimeUtilDeclension() { }
}
