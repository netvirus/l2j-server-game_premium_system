package com.l2jserver.gameserver.instancemanager;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.model.L2PremiumBonus;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.Containers;
import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.ListenersContainer;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogout;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;
import com.l2jserver.gameserver.network.serverpackets.ExBrPremiumState;
import com.l2jserver.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.util.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.function.Consumer;

import static com.l2jserver.gameserver.config.Configuration.customs;

/**
 * Premium system manager
 * @author netvirus
 */

public class PremiumSystemManager {

    private static final Logger LOG = LoggerFactory.getLogger(PremiumSystemManager.class);
    private final Map<Integer, ScheduledFuture<?>> expiretasks = new HashMap<>();

    // Get listeners
    private final ListenersContainer listenerContainer = Containers.Players();

    protected PremiumSystemManager()
    {
        listenerContainer.addListener(new ConsumerEventListener(listenerContainer, EventType.ON_PLAYER_LOGIN, playerLoginEvent, this));
        listenerContainer.addListener(new ConsumerEventListener(listenerContainer, EventType.ON_PLAYER_LOGOUT, playerLogoutEvent, this));
    }

    private final Consumer<OnPlayerLogin> playerLoginEvent = (event) ->
    {
        Map<Boolean, L2PremiumBonus> premiums = DAOFactory.getInstance().getPremiumSystemDAO().load(event.getActiveChar().getObjectId());
        L2PcInstance player = event.getActiveChar();
        if (!premiums.isEmpty()) {
            L2PremiumBonus premium = null;
            switch (premiums.size()) {
                // Have one premium
                case 1: {
                    premium = premiums.get(premiums.keySet().stream().findFirst().get());
                    player.setTwoPremium(false);
                    break;
                }
                // Have two premium
                case 2: {
                    premium = premiums.get(false);
                    player.setTwoPremium(true);
                    break;
                }
            }
            // Enable premium status
            enablePremiumStatus(player, premium);
        } else {
            // Disable premium status
            disablePremiumStatus(player);
        }
        premiums.clear();
    };

    private final Consumer<OnPlayerLogout> playerLogoutEvent = (event) ->
    {
        //TODO Implement update time for not main premium
        //stopExpireTask(player);
    };

    private void startExpireTask(L2PcInstance player, long delay)
    {
        //final ScheduledFuture<?> task = ThreadPoolManager.getInstance().scheduleEvent(new PremiumExpireTask(player), delay);
        //expiretasks.put(player.getObjectId(), task);
    }

    private void stopExpireTask(L2PcInstance player)
    {
//        ScheduledFuture<?> task = expiretasks.remove(player.getAccountName());
//        if (task != null)
//        {
//            task.cancel(false);
//            task = null;
//        }
    }

    private void enablePremiumStatus(L2PcInstance player, L2PremiumBonus premium, boolean showVisualEffect) {
        enablePremiumStatus(player, premium);
        player.broadcastPacket(new MagicSkillUse(player, player, 6463, 1, 0, 0));
    }

    private void enablePremiumStatus(L2PcInstance player, L2PremiumBonus premium) {
        long timer = premium.getBonusDuration();
        setPremiumStatus(player, premium, true);
        startExpireTask(player, ((timer * 1000L) - System.currentTimeMillis()));
        String premiumMsg = "Your premium subscription will expire in: " + TimeUtils.formatTime((int) (timer - (System.currentTimeMillis() / 1000)));
        player.sendPacket(new ExShowScreenMessage(premiumMsg, ExShowScreenMessage.TOP_CENTER, 7000));
    }

    private void disablePremiumStatus(L2PcInstance player) {
        setPremiumStatus(player, new L2PremiumBonus(), false);
        player.setTwoPremium(false);
    }

    /*
     * @param player - active char
     * @param premium is the object L2PremiumBonus
     * @param boolean state of premium
     */
    private void setPremiumStatus(L2PcInstance player, L2PremiumBonus premium, boolean premiumState) {
        player.setPremiumStatus(premiumState);
        player.setPremiumBonus(premium);
        player.sendPacket(new ExBrPremiumState(player.getObjectId(), premiumState));
    }

    public static PremiumSystemManager getInstance() {
        return PremiumSystemManager.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        protected static final PremiumSystemManager INSTANCE = new PremiumSystemManager();
    }
}
