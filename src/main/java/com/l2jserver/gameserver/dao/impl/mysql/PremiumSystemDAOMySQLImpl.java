package com.l2jserver.gameserver.dao.impl.mysql;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PremiumSystemDAO;
import com.l2jserver.gameserver.data.xml.impl.PremiumSystemOptionsData;
import com.l2jserver.gameserver.model.L2PremiumBonus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Premium system DAO MySQL implementation
 * @author netvirus
 */

public class PremiumSystemDAOMySQLImpl implements PremiumSystemDAO {

    private static final Logger LOG = LoggerFactory.getLogger(PremiumSystemDAOMySQLImpl.class);

    private final Map<Boolean, L2PremiumBonus> premiumType = new HashMap<>();

    private static final String SELECT = "SELECT * FROM premium_system WHERE char_id=? AND active=1";
    private static final String INSERT = "INSERT INTO premium_system (char_id, bonus_id, bonus_expire) VALUES (?,?,?)";
    private static final String DISABLE = "UPDATE premium_system SET active=0 WHERE id=?";

    @Override
    public Map load(int objectId) {
        try (var con = ConnectionFactory.getInstance().getConnection();
             var ps = con.prepareStatement(SELECT)) {
            ps.setInt(1, objectId);
            try (var rset = ps.executeQuery()) {
                if (rset.next()) {
                    if (rset.getLong("bonus_expire") > (System.currentTimeMillis() / 1000L))
                    {
                        L2PremiumBonus premium = new L2PremiumBonus(PremiumSystemOptionsData.getInstance().findById(rset.getInt("bonus_id")));
                        premium.setBonusDuration(rset.getLong("bonus_expire"));
                        premiumType.put(premium.isBonusMain(), premium);
                    }
                    else
                    {
                        disable(rset.getInt("id"));
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Failed loading premium data. {}", e);
        }
        return premiumType;
    }

    @Override
    public void insert(int charId, int bonusId, long duration) {
        try (var con = ConnectionFactory.getInstance().getConnection();
             var ps = con.prepareStatement(INSERT)) {
            ps.setInt(1, charId);
            ps.setInt(2, bonusId);
            ps.setLong(3, duration);
            ps.executeQuery();
        } catch (Exception e) {
            LOG.error("Failed insert premium data. {}", e);
        }
    }

    @Override
    public void disable(int id) {
        try (var con = ConnectionFactory.getInstance().getConnection();
             var ps = con.prepareStatement(DISABLE)) {
            ps.setInt(1, id);
            ps.executeQuery();
        } catch (Exception e) {
            LOG.error("Failed disable premium data. {}", e);
        }
    }
}
