package com.l2jserver.gameserver.dao;

import com.l2jserver.gameserver.model.L2PremiumBonus;

import java.util.Map;

/**
 * Premium System DAO Interface
 * @author netvirus
 */

public interface PremiumSystemDAO {

    /**
     * Restores player premium state from the database.
     * @param objectId the player's object ID
     * @return list of premium for player
     */
    Map load(int objectId);

    /**
     * Add new premium for player in the database.
     * @param charId -> player object id
     * @param bonusId -> L2PremiumBonus.bonusId
     * @param duration -> Time now + L2PremiumBonus.duration
     */
    void insert(int charId, int bonusId, long duration);

    /**
     * Disables premium player status in the database.
     * @param id the player's record ID in the database
     */
    void disable(int id);
}